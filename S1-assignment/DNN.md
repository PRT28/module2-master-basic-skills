# DNN Summary
## What is a neural network?
A neuron is a unit which holds a number known as activation. These neuron grouped together as layer are known as neural layer. When several of these layers are connected to each other are known as neural network. If the numbers of layers are less then the networks is called shallow neural network and if number of layers are large then its called Deep Neural Network(DNN)
![DNN](https://miro.medium.com/max/800/1*AGVQTf-SKl0drfwmusaCww.jpeg)

Different neuron in layers learn different instances for example edges in computer vision. The activations in each layers gives the values of the brightness of pixel with 0 equals black and 1 equals white.

![DNN](https://miro.medium.com/max/796/1*dcUIVHFbRQenMjihwrjqcQ.jpeg)

The activation function is a step function that turns the neuron output on and off, depending on a rule or threshold

![DNN](https://missinglink.ai/wp-content/uploads/2018/11/activationfunction-1.png)

The Different Types of activations are:
- Sigmoid:![Sigmoid](https://missinglink.ai/wp-content/uploads/2018/11/sigmoidlogisticgraph.png)
- Tanh:   ![Tanh](https://missinglink.ai/wp-content/uploads/2018/11/tanhhyperbolic.png)
- Relu:   ![Relu](https://missinglink.ai/wp-content/uploads/2018/11/relu.png)
## Gradient descent, how neural networks learn
Gradient Descent is a process that occurs in the backpropagation phase where the goal is to minimize the cost function.
![GD](https://miro.medium.com/max/875/1*bP7EaVyPvBSRIxqgA9TbGQ.png)

- __LOSS__:Function that defines how similar is the predicted output is to the true output(Reffered to one training step in a batch).
- __COST__:The total loss over a complete training batch is called cost.

Neural networks build knowledge from data sets where the right answer is provided in advance. The networks then learn by tuning themselves to find the right answer on their own, increasing the accuracy of their predictions.

![NN](https://www.kdnuggets.com/wp-content/uploads/fast-forward-labs-neural-net.jpg)

Different parametes(W and b) and hyperparameters are tuned when the training occurs which also updates the activations stored in each neuron thus increasing the overall accuracy.
## What is backpropagation really doing?
During backpropagation we feed the losses to our neurons and tune the activations and parameters of neurons, this is called optimization.

![BackProp](https://miro.medium.com/max/625/0*ETudkFMzVEMsUrVD.png)

Different types of Gradient Descent are:
- Stochastic Gradient Descent
- Batch Gradient Descent
- Mini-batch Gradient Descent
#### Stochastic gradient descent
Stochastic gradient descent refers to calculating the derivative from each training data instance and calculating the update immediately.

![SGD](https://www.mltut.com/wp-content/uploads/2020/04/Untitled-document-3.png)