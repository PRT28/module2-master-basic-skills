# CNN Summary
CNN are one of the most used neural layers in Computer Vision, used for different tasks  such as object detection, image classification, face recognition, etc.

![CNN](https://stanford.edu/~shervine/teaching/cs-230/illustrations/architecture-cnn-en.jpeg?3b7fccd728e29dc619e1bd8022bf71cf)

## Convoltuion Process

![CNN](https://sds-platform-private.s3-us-east-2.amazonaws.com/uploads/75_blog_image_1.png)

The CNN layers extract and learn the features of the images provided to them using different filters.
Activation functions used on CNN defines whther to activate neuron or not.

![CNN](https://stanford.edu/~shervine/teaching/cs-230/illustrations/convolution-layer-a.png?1c517e00cb8d709baf32fc3d39ebae67)

__Pooling layers__ are used which downsample the features learned to reduce the parameters for the next layer.

![CNN](https://stanford.edu/~shervine/teaching/cs-230/illustrations/max-pooling-a.png?711b14799d07f9306864695e2713ae07)![CNN](https://stanford.edu/~shervine/teaching/cs-230/illustrations/average-pooling-a.png?58f9ab6d61248c3ec8d526ef65763d2f)

Once the features has been learned by the CNN then a __Fully Connected__ layer is Connected.

![CNN](https://stanford.edu/~shervine/teaching/cs-230/illustrations/fully-connected-ltr.png?32caf9e07c79d652faa292812579d063)

### Important Concepts:
- __Kernel Size(k)__ is the size of filters used on the CNN to learn features.
![K](https://images.ctfassets.net/be04ylp8y0qc/4CZKbDvPaWKbxO00WX3elB/a4a51727768b2948e42377582ba4df3d/kernel3.png?fm=webp)
- __Stride(s)__ is the number of pixels left by the filters after each step of convolution.
![s](https://stanford.edu/~shervine/teaching/cs-230/illustrations/stride.png?36b5b2e02f7e02c3c4075a9d836c048c)
- __Zero Padding(pad)__ is the amount of pixels added at the edges of input so that after Convoltuion the output size does not decreses. The added pixel have value 0 hence known as Zero Padding.

![pad](https://stanford.edu/~shervine/teaching/cs-230/illustrations/padding-full-a.png?b51e98467c8a77574c7e8f108654ad95)