# Object Detection
Object detection is a computer vision technique that works to identify and locate objects within an image or video. Specifically, it draws bounding boxes around these detected objects.

![OD](https://miro.medium.com/max/875/1*spGG34X1S9MlW512UQ86jA.png)

Sliding window technique is used where we take all possible crops of the image, then image classification is performed, if an object is recognized above a certain threshold value then a bounding box is drawn.

![OD](https://miro.medium.com/max/875/1*zbBVqsIM9eYiQpG5LZ5Opw.gif)

With this technique we may get multiple bounding boxesfor single object. These are elemented by intersection-over-union (IOU). In this we compute the area of the intersection and divide by the area of the union. This value ranges from 0 (no interaction) to 1 (perfectly overlapping)

![OD](https://www.fritz.ai/images/ssd.jpg)